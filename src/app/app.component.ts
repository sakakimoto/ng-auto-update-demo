import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AutoUpdateDemo';
  input: {
    number: number;
    string: string;
  }
  
  submit() {
    console.log('submit', this.input)
  }
}
